import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { LoadingService } from '../services/loading.service';

@Component({
	selector: 'app-terms-conditions',
	templateUrl: './terms-conditions.page.html',
	styleUrls: ['./terms-conditions.page.scss'],
})
export class TermsConditionsPage implements OnInit {

	errors:any
	terms:any;
	constructor(
		public loading: LoadingService,
		public api: ApiService,
	) { }

	ngOnInit() {
	}

	ionViewWillEnter() {
		this.getLoginTerms();
	}

	async getLoginTerms() {
		// await this.loading.open();
		await this.api.open('getLoginTerm', '').then((response) => {
			// this.loading.close();
			this.terms = response['data']
		}, (error) => {
			// this.loading.close();
			this.errors = error.error.errors;
		});
	}

}
