import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { StatusBar, Style } from '@capacitor/status-bar';
import { SplashScreen } from '@capacitor/splash-screen';
import { register } from 'swiper/element/bundle';
import { ApiService } from './services/api.service';
import { LoadingService } from './services/loading.service';
import { ActionPerformed, PushNotificationSchema, PushNotifications, Token, } from '@capacitor/push-notifications';
register();
@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: ['app.component.scss'],
})
export class AppComponent {
	current_version = '2.0.2'
	errors: any;
	customer: any
	constructor(
		private storage: Storage,
		public api: ApiService,
		public loading: LoadingService,
		private platform: Platform,
	) {
		this.storage.create();
	}

	async ngOnInit() {
		await SplashScreen.hide();
		await SplashScreen.show({
			showDuration: 1000,
			autoHide: true,
		});
		await this.storage.get('customer').then((data) => {
			this.customer = data
			this.initializeApp()
		})
	}

	async initializeApp() {
		await this.platform.ready().then(() => {
			// this.getCurrentVersion();
			this.initializeFcm()
			this.requestPermission();
		})
		StatusBar.setBackgroundColor({ color: '#000000' }).then(async () => {
			await StatusBar.setStyle({ style: Style.Default });
			await StatusBar.setOverlaysWebView({ overlay: false })
		}, err => {
			console.log(err)
		})
	}

	async getCurrentVersion() {
		await this.api.open('getVersion', "").then((response) => {
			if (this.current_version !== response['data']) {
				window.open('https://play.google.com/store/apps/details?id=club.goldenaces.app')
			}
		}, (error) => {
			this.errors = error.error.errors;
		});
	}

	requestPermission() {
		this.api.requestPermission().subscribe(
			async token => {
				this.listenForMessages();
				if (this.customer) {
					this.sendToken(token, this.customer['customer_id'])
				}
				else {
					this.storage.set('fcm_token', token)
				}
			}
		)
	}

	listenForMessages() {
		this.api.getMessages().subscribe(async (msg: any) => {
			console.log(msg)
		}, err => {
			console.log(err)
		});
	}

	initializeFcm() {
		PushNotifications.requestPermissions().then(result => {
			if (result.receive === 'granted') {
				console.log('granted', result)
				// Register with Apple / Google to receive push via APNS/FCM
				PushNotifications.register();
			} else {
				console.log('not granted', result)
			}
		}, err => {
			console.log(err)
		});

		PushNotifications.addListener('registration', (token: Token) => {
			if (this.customer) {
				this.sendToken(token.value, this.customer['customer_id'])
			}
			else {
				this.storage.set('fcm_token', token.value)
			}
		});

		PushNotifications.addListener('registrationError', (error: any) => {
			console.log('Error on registration: ', error)
		});

		PushNotifications.addListener(
			'pushNotificationReceived',
			(notification: PushNotificationSchema) => {
				console.log('Push received: ', notification)
			},
		);

		PushNotifications.addListener(
			'pushNotificationActionPerformed',
			(notification: ActionPerformed) => {
				console.log('Push action performed:', notification)
			},
		);
	}

	async sendToken(fcm_token, customer_id) {
		await this.api.httpCall('updateFCMToken', { 'fcm_token': fcm_token, 'customer_id': customer_id }).then((response: any) => {
			console.log(response)
		}, error => {
			console.log(error)
		})
	}
}
