import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePage } from './profile.page';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage
  },
  {
    path: 'basic-details',
    loadChildren: () => import('./basic-details/basic-details.module').then( m => m.BasicDetailsPageModule)
  },
  {
    path: 'manage-notifications',
    loadChildren: () => import('./manage-notifications/manage-notifications.module').then( m => m.ManageNotificationsPageModule)
  },
  {
    path: 'choose-language',
    loadChildren: () => import('./choose-language/choose-language.module').then( m => m.ChooseLanguagePageModule)
  },
  {
    path: 'about-us',
    loadChildren: () => import('./about-us/about-us.module').then( m => m.AboutUsPageModule)
  },
  {
    path: 'help-faqs',
    loadChildren: () => import('./help-faqs/help-faqs.module').then( m => m.HelpFaqsPageModule)
  },
  {
    path: 'update-kyc',
    loadChildren: () => import('./update-kyc/update-kyc.module').then( m => m.UpdateKycPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilePageRoutingModule {}
