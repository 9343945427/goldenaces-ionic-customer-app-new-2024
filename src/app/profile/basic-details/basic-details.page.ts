import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
@Component({
	selector: 'app-basic-details',
	templateUrl: './basic-details.page.html',
	styleUrls: ['./basic-details.page.scss'],
})
export class BasicDetailsPage implements OnInit {
	customer: any;
	constructor(
		private storage: Storage,
		public loading: LoadingService,
		public api: ApiService,
		private actionSheetController: ActionSheetController,
		private route: ActivatedRoute,
		public router: Router,
	) { }

	ngOnInit() {
	}

	async ionViewWillEnter() {
		await this.storage.get("customer").then((data) => {
			if (data !== null) {
				this.customer = data;
			}
		});
	}

	async update(customer) {
		await this.loading.open();
		await this.api.httpCall('profile', this.customer).then((response) => {
			this.loading.close();
			this.storage.set('customer', response['data']);
			this.router.navigate(['/tabs/account']);
			this.loading.presentAlert("Message", "Your profile has been Updated successfully.")
		}, (error) => {
			this.loading.close();
			this.router.navigate(['/profile']);
			console.log(error)
		});
	}
}
