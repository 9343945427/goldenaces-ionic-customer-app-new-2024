import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HelpFaqsPage } from './help-faqs.page';

const routes: Routes = [
  {
    path: '',
    component: HelpFaqsPage
  },
  {
    path: 'view',
    loadChildren: () => import('./view/view.module').then( m => m.ViewPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HelpFaqsPageRoutingModule {}
