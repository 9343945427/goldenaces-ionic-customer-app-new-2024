import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'app-view',
	templateUrl: './view.page.html',
	styleUrls: ['./view.page.scss'],
})
export class ViewPage implements OnInit {
	@Input() term: any;
	constructor(
		private modalCtrl: ModalController
	) { }

	ngOnInit() {
	}

	close() {
		this.modalCtrl.dismiss()
	}

}
