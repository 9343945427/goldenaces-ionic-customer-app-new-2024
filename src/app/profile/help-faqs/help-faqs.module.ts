import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HelpFaqsPageRoutingModule } from './help-faqs-routing.module';

import { HelpFaqsPage } from './help-faqs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HelpFaqsPageRoutingModule
  ],
  declarations: [HelpFaqsPage]
})
export class HelpFaqsPageModule {}
