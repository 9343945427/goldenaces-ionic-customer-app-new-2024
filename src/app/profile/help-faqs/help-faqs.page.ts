import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { ViewPage } from './view/view.page';
@Component({
	selector: 'app-help-faqs',
	templateUrl: './help-faqs.page.html',
	styleUrls: ['./help-faqs.page.scss'],
})
export class HelpFaqsPage implements OnInit {
	terms: any;
	view_all = false
	constructor(
		private storage: Storage,
		public loading: LoadingService,
		public api: ApiService,
		private modalCtrl: ModalController
	) { }

	ngOnInit() {
		this.getTerms()
	}

	async getTerms() {
		await this.loading.open()
		await this.api.httpCall('getTerms', 'Rules').then(response => {
			this.loading.close()
			this.terms = response['data']
		}, (error) => {
			this.loading.close();
			console.log(error);
		});
	}

	viewAll() {
		this.view_all == true ? this.view_all = false : this.view_all = true
	}

	async viewTerms(term) {
		const modal = await this.modalCtrl.create({
			component: ViewPage,
			componentProps: {
				term: term
			}
		});
		modal.present();
	}
}
