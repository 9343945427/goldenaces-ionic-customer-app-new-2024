import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HelpFaqsPage } from './help-faqs.page';

describe('HelpFaqsPage', () => {
  let component: HelpFaqsPage;
  let fixture: ComponentFixture<HelpFaqsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(HelpFaqsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
