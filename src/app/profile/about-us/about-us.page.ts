import { Component, OnInit, ViewChild } from '@angular/core';
import { IonModal } from '@ionic/angular';

@Component({
	selector: 'app-about-us',
	templateUrl: './about-us.page.html',
	styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage implements OnInit {
	@ViewChild('aboutUs') about_us: IonModal;
	@ViewChild('privacy') privacy: IonModal;
	@ViewChild('term') term: IonModal;
	show = false
	constructor() { }

	ngOnInit() {
	}

	closeAboutUs() {
		this.about_us.dismiss(null, 'cancel');
	}

	closePrivacy() {
		this.privacy.dismiss(null, 'cancel');
	}

	closeTerm() {
		this.term.dismiss(null, 'cancel');
	}

	rotate() {
		document.getElementById('img').attributes['getNamedItem']
		if (document.getElementById('img').style.transform == 'rotate(-90deg)') {
			document.getElementById('img').style.transform = 'rotate(0deg)'
			this.show = true
		} else {
			document.getElementById('img').style.transform = 'rotate(-90deg)'
			this.show = false
		}
	}
}
