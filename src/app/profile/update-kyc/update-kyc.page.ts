import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { AlertController, NavController, ModalController, Platform, ActionSheetController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Storage } from '@ionic/storage';
@Component({
	selector: 'app-update-kyc',
	templateUrl: './update-kyc.page.html',
	styleUrls: ['./update-kyc.page.scss'],
})
export class UpdateKycPage implements OnInit {

	@ViewChild('panInput') panInputViewChild;
	@ViewChild('selfieInput') selfieInputViewChild: ElementRef;
	@ViewChild('aadhaarInput') aadhaarInputViewChild: ElementRef;
	@ViewChild('imagePan') imagePan: ElementRef;
	userPanElement: HTMLInputElement;
	userSelfieElement: HTMLInputElement;
	userAadhaarElement: HTMLInputElement;
	customer: any
	pan: any
	panImage: any
	selfie: any;
	selfieImage: any
	aadhaar: any;
	aadhaarImage: any
	meta = {
		pan: null,
		aadhaar: null,
		selfie: null,
		customer_id: null,
		source: null
	}
	doc_type: any;
	request_id: any;
	panStatus: any;
	docStatus: any
	kycType: any;
	errors: any;
	segmentModel = 'pan'
	alert = ""
	bankDetails = {
		bank_account_no: null,
		confirm_account_no: null,
		bank_ifsc_code: null,
		name_at_bank: null
	}
	status = {
		pan: '',
		aadhaar: '',
		bank: ''
	}
	updateForm: FormGroup;
	validations = {
		'bank_account_no': [
			{ type: 'required', message: 'Account number is required.' },
		],
		'confirm_account_no': [
			{ type: 'required', message: 'Confirm Account number is required.' },
		],
		'bank_ifsc_code': [
			{ type: 'required', message: 'IFSC code is required.' },
		],
	};
	myImage = null;
	constructor(
		public loading: LoadingService,
		public api: ApiService,
		private storage: Storage,
		private domSanitizer: DomSanitizer,
		private alertController: AlertController,
		private navctrl: NavController,
		private router: Router,
		public modalCtrl: ModalController,
		public platform: Platform,
		// private camera: Camera,
		private actionSheetController: ActionSheetController,
		// private iab: InAppBrowser

	) {
		this.updateForm = new FormGroup({
			'bank_account_no': new FormControl('', Validators.compose([
				Validators.required,
			])),
			'confirm_account_no': new FormControl('', Validators.compose([
				Validators.required,
			])),
			'bank_ifsc_code': new FormControl('', Validators.compose([
				Validators.required,
			])),
			'customer_id': new FormControl('', Validators.compose([
				Validators.required,
			])),
		});

	}

	ngOnInit() {

	}

	async ionViewWillEnter() {
		await this.storage.get('customer').then((data) => {
			if (data !== null) {
				this.customer = data;
				this.meta.customer_id = data['customer_id']
				this.getStatus(this.meta.customer_id, this.segmentModel)
			}
		});
	}

	ngAfterViewInit() {
		this.userPanElement = this.panInputViewChild.nativeElement;
		this.userSelfieElement = this.selfieInputViewChild.nativeElement;
		this.userAadhaarElement = this.aadhaarInputViewChild.nativeElement;
	};


	selectpan() {
		this.userPanElement.click()
	}

	selectselfie() {
		this.userSelfieElement.click()
	}

	selectaadhaar() {
		this.userAadhaarElement.click()
	}

	//load documents
	segmentChanged(ev) {
		this.userSelfieElement = this.selfieInputViewChild.nativeElement;
		this.userPanElement = this.panInputViewChild.nativeElement;
		this.userAadhaarElement = this.aadhaarInputViewChild.nativeElement;
		this.doc_type = ev.target.value
		this.getStatus(this.meta.customer_id, ev.target.value)
	}

	readFileAsBase64(file) {
		return new Promise((resolve, reject) => {
			const fileReader = new FileReader();
			fileReader.onload = () => resolve(fileReader.result);
			fileReader.readAsDataURL(file);
		});
	}

	async capturePhotos() {
		const capturedPhoto = await Camera.getPhoto({
			resultType: CameraResultType.Base64,
			source: CameraSource.Camera,
			quality: 10
		});
		let img = 'data:image/jpeg;base64,' + capturedPhoto.base64String
		this.myImage = img;
		this.meta.pan = this.myImage;
		this.meta.source = 'camera';
	}

	async selectMedia() {
		const actionSheet = await this.actionSheetController.create({
			header: 'Select one option',
			buttons: [
				{
					text: 'Capture Image',
					icon: 'camera-outline',
					handler: () => {
						this.capturePhotos();
					}
				},
				{
					text: 'Gallery',
					icon: 'images-outline',
					handler: () => {
						this.selectpan();
					}
				},
				{
					text: 'Cancel',
					icon: 'close-circle-outline',
					role: 'cancel'
				}
			]
		});
		await actionSheet.present();
	}

	loadPan(event) {
		const file = event.target.files[0];
		let fileSize = file.size / 1000000
		this.pan = file
		if (file.type === 'video/mp4' || file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
			this.fileTypeAlert()
		}
		if (fileSize >= 5) {
			this.fileSizeAlert()
		} else {
			this.readFileAsBase64(file).then(res => {
				this.meta.pan = res
			}, err => {
				console.log(err)
			})
		}
	}

	returnPan(pan) {
		return this.domSanitizer.bypassSecurityTrustResourceUrl(pan)
	}

	loadSelfie(event) {
		const file = event.target.files[0];
		let fileSize = file.size / 1000000
		this.selfie = file
		if (file.type === 'video/mp4' || file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
			this.fileTypeAlert()
		}
		if (fileSize >= 5) {
			this.fileSizeAlert()
		} else {
			this.readFileAsBase64(file).then(res => {
				this.meta.selfie = res
				this.selfieImage = this.meta.selfie
			})
		}
	}

	loadAadhaar(event) {
		const file = event.target.files[0];
		let fileSize = file.size / 1000000
		this.aadhaar = file
		if (file.type === 'video/mp4' || file.type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
			this.fileTypeAlert()
		}
		if (fileSize >= 5) {
			this.fileSizeAlert()
		} else {
			this.readFileAsBase64(file).then(res => {
				this.meta.aadhaar = res
				this.aadhaarImage = this.meta.aadhaar
			})
		}
	}

	removeFile(type) {
		if (type == 'pan') {
			this.panInputViewChild.nativeElement.value = null;
			this.userPanElement.nodeValue = ""
			this.meta.pan = null
			this.pan = null
			this.meta.source = ""
		}
		if (type == 'aadhaar') {
			this.aadhaarInputViewChild.nativeElement.value = null;
			this.userAadhaarElement.nodeValue = ""
			this.meta.aadhaar = null
			this.aadhaar = null
		}
		if (type == 'selfie') {
			this.selfieInputViewChild.nativeElement.value = null;
			this.userSelfieElement.nodeValue = ""
			this.meta.selfie = null
			this.selfie = null
		}
	}

	//Alerts
	async fileTypeAlert() {
		const alert = await this.alertController.create({
			header: 'Alert',
			message: 'Please select PDF/PNG/JPEG files only.',
			cssClass: 'my-custom-class',
			backdropDismiss: false,
			buttons: [
				{
					text: 'OK',
					role: 'confirm',
					handler: () => {
						location.reload()
					},
				},
			],
		});
		await alert.present();
	}

	async fileSizeAlert() {
		const alert = await this.alertController.create({
			header: 'Alert',
			message: 'File size should be less than 5MB.',
			cssClass: 'my-custom-class',
			buttons: [
				{
					text: 'OK',
					role: 'confirm',
					handler: () => {
						location.reload()
					},
				},
			],
		});
		await alert.present();
	}

	//Upload Documents//
	async sendPan() {
		await this.loading.open()
		await this.api.httpCall('idfy/verifyPan', this.meta).then((response) => {
			this.loading.close()
			this.request_id = response
			if (response != null) {
				this.panInputViewChild.nativeElement.value = null;
				this.getTask(this.request_id)
			}
		}, (error) => {
			this.loading.close()
			this.loading.presentAlert("Verification Failed!", error.error.message)
			this.removeFile('pan')
		});
	}

	sendAadhaar() {
		this.segmentModel = 'pan'
		this.api.httpCall('idfy/verifyAadhaar', this.meta).then((response) => {
			var windowReference = window.open();
			windowReference.location = response['redirect_url']
		}, (error) => {
			this.removeFile('aadhaar')
			this.loading.presentAlert("Verification Failed!", error.error.message)
		});
	}

	async updateBankDetails() {
		let data = {
			customer_id: this.meta.customer_id,
			data: this.updateForm.value,
			bank_account_no: this.updateForm.value.bank_account_no,
			bank_ifsc_code: this.updateForm.value.bank_ifsc_code,
		}
		if (this.updateForm.value.bank_account_no != this.updateForm.value.confirm_account_no) {
			this.errors = 1
		} else {
			this.errors = null
			await this.loading.open();
			await this.api.httpCall('idfy/verifyBank', data).then((response) => {
				this.loading.close();
				if (response != null) {
					this.getTask(response)
				}
			}, (error) => {
				this.loading.close()
				this.errors = error.error.errors;
				this.loading.presentAlert("Verification Failed!", error.error.message)
			});
		}
	}

	async getTask(document) {
		await this.loading.open();
		await this.api.httpCall('idfy/getTask', document).then((response) => {
			this.loading.close();
			this.storage.set('customer', response['data']);
			if (this.segmentModel == 'aadhaar') {
				this.switchToPan()
			} else {
				this.loading.presentAlert("Upload Successfull", "Document verified successfully")
				this.getStatus(document.customer_id, document.document_type)
			}
		}, (error) => {
			this.loading.close()
			this.errors = error.error;
			if (this.errors) {
				this.loading.presentAlert("Message", error.error.message)
				if (error.error.status == 'Try') {
					this.docStatus = 'alert'
					this.alert = error.error.message;
				}
				else {
					this.docStatus = 'upload'
				}
			}
		});
	}

	async getStatus(customer_id, doc_type) {
		let meta = {
			customer_id: customer_id,
			document_type: doc_type
		}
		await this.loading.open();
		await this.api.httpCall('idfy/getStatus', meta).then((response) => {
			this.loading.close();
			this.storage.set('customer', response['customer']);
			this.docStatus = response['status']
			if (response['status'] == 'task') {
				let document = response['document']
				this.getTask(document)
			}
			if (response['status'] == 'display' && response['document'].document_type == 'pan') {
				this.pan = response['document']
				console.log(this.pan)
			}
			if (response['status'] == 'display' && response['document'].document_type == 'aadhaar') {
				let aadhaar = JSON.parse(response['document'].response)
				this.aadhaar = aadhaar.parsed_details
			}
			if (response['status'] == 'display' && response['document'].document_type == 'bank') {
				let bank = JSON.parse(response['document'].response)
				this.bankDetails.bank_account_no = bank.result.bank_account_number
				this.bankDetails.name_at_bank = bank.result.name_at_bank
				this.bankDetails.bank_ifsc_code = bank.result.bank_ifsc_code
			}
		}, (error) => {
			this.errors = error.error.errors;
			this.loading.close()
		});
	}

	async switchToPan() {
		const alert = await this.alertController.create({
			cssClass: 'alert',
			header: 'Upload Successfull !',
			message: 'Document verified successfully',
			buttons: [
				{
					text: 'Ok',
					id: 'confirm-button',
					handler: () => {
						if (this.segmentModel == 'aadhaar') this.segmentModel = 'pan'
					}
				}
			]
		});
		await alert.present();
	}

	doRefresh(event) {
		this.ionViewWillEnter().then(() => {
			event.target.complete();
		})
	}

	openWithInAppBrowser(url, document) {
		// this.segmentModel = 'pan'
		// Browser.open({ url: url }).then(async (res) => { });
		// Browser.addListener('browserFinished', () => this.getTask(document))
		// let browser = this.iab.create(url, '_self');
		// browser.on('exit').subscribe(event => {
		// 	console.log(event)
		// 	this.getTask(document)
		// });
	}

	async confirmChangeBankAccount() {
		const alert = await this.alertController.create({
			cssClass: 'alert',
			message: 'Are you sure?',
			subHeader: '',
			mode: 'ios',
			buttons: [
				{
					text: 'No',
					id: 'close',
					handler: () => {
					}
				},
				{
					text: 'Yes',
					id: 'confirm-button',
					handler: () => {
						this.changeBankAccount()
					}
				}
			]
		});
		await alert.present();
	}

	async changeBankAccount() {
		let payload = {
			customer_id: this.meta.customer_id
		}
		await this.loading.open();
		await this.api.httpCall('idfy/deleteBankDocument', payload).then((response) => {
			this.loading.close();
			this.getStatus(this.meta.customer_id, this.segmentModel)
		}, (error) => {
			this.loading.close()
		});
	}

}
