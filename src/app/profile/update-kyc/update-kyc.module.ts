import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateKycPageRoutingModule } from './update-kyc-routing.module';

import { UpdateKycPage } from './update-kyc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    UpdateKycPageRoutingModule
  ],
  declarations: [UpdateKycPage]
})
export class UpdateKycPageModule {}
