import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ApiService } from '../services/api.service';
import { LoadingService } from '../services/loading.service';
import { Router, NavigationExtras } from '@angular/router';
import { Share } from '@capacitor/share';
@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
    customer: any;
    errors: any;
    logStatus: any;
    log: any
    referral_code: any
    description = 'You have been invited to join the Golden Aces club. Please register at Golden Aces using the referral code : '
    share_via = ''
    constructor(
        private storage: Storage,
        public loading: LoadingService,
        public api: ApiService,
        public alertController: AlertController,
        public router: Router,
        private navCtrl: NavController
    ) { }

    ngOnInit() {
        
    }

    ionViewWillEnter() {
        this.storage.get("customer").then((data) => {
            if (data !== null) {
                this.customer = data;
                this.checkLogStatus();
            }
        });
    }
    
    async checkLogStatus() {
        let payload = {
            customer_id: this.customer.customer_id,
            log_type: 'Customer'
        };
        await this.api.httpCall("logStatus", payload).then(
            (response) => {
                this.logStatus = response["status"];
                this.log = response["log"];
                this.storage.set('branch', response["log"].branch)
                if (this.customer?.referral_status == true) this.getReferalCode()

            },
            (error) => {
                this.errors = error.error.errors;
            }
        );
    }

    async generateQRCode() {
        //Without KYC Status
        let payload = {
            customer_id: this.customer.customer_id,
            log_type: 'Customer'
        };
        await this.api.httpCall("generateQRCode", payload).then(
            (response) => {
                let navigationExtras: NavigationExtras = {
                    state: {
                        qrcode: response["data"].code,
                    },
                };
                this.router.navigate(["/home/qrcode"], navigationExtras);
            },
            (error) => {
                this.presentAlert("Account Disabled", error.error.message[0]);
                this.errors = error.error.errors;
            }
        );
    }

    copy() {
        navigator.clipboard.writeText(this.referral_code);
        this.loading.message('Referral Code Copied!')
    }
    async checkOut() {
        let payload = {
            log_id: this.log.log_id,
        };
        await this.loading.open();
        await this.api.httpCall("checkOut", payload).then(
            (response) => {
                this.loading.close();
                this.logStatus = "CheckIn";
                this.log = null;
                this.storage.set('branch', null)
                this.loading.presentAlert("Check-Out", "Thank you for your time at Golden Aces Club. We hope to see you again!");
            },
            (error) => {
                this.loading.close();
                this.errors = error.error.errors;
            }
        );
    }

    async checkOutConfirm() {
        const alert = await this.alertController.create({
            cssClass: 'alert',
            header: 'Confirm !',
            message: 'Are you sure ?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'alert',
                    id: 'cancel-button',
                    handler: () => {

                    }
                }, {
                    text: 'Check-Out',
                    id: 'confirm-button',
                    handler: () => {
                        this.checkOut();
                    }
                }
            ]
        });
        await alert.present();
    }

    async getReferalCode() {
        await this.api.httpCall('generateReferralCode', this.customer).then(response => {
            this.referral_code = response['data'].referral_code
        }, (error) => {
            console.log(error)
        });
    }

    async presentAlert(title, message) {
        const alert = await this.alertController.create({
            cssClass: "alert",
            header: title,
            message: message,
            buttons: ["OK"],
        });
        alert.onDidDismiss().then(() => {
            this.router.navigate(["/profile"]);
        });
        await alert.present();
    }

    async selectShareOption() {
        await Share.share({
            title: 'Referral',
            dialogTitle: 'Share with buddies',
            text: this.description + '' + this.referral_code,
        });
    }

    async logoutConfirm() {
        const alert = await this.alertController.create({
            cssClass: 'alert',
            header: 'Confirm !',
            message: 'Are you sure ?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'alert',
                    id: 'cancel-button',
                    handler: () => {
                        return false
                    }
                }, {
                    text: 'Logout',
                    id: 'confirm-button',
                    handler: () => {
                        this.logout()
                    }
                }
            ]
        });
        await alert.present();
    }

    async logout() {
        await this.loading.open();
        this.storage.remove('customer')
        this.storage.remove('token')
        this.storage.remove('code')
        this.storage.remove('user')
        this.storage.remove('logStatus')
        this.storage.remove('branch')
        this.navCtrl.navigateRoot('/signup');
        this.loading.close()
    }
}
