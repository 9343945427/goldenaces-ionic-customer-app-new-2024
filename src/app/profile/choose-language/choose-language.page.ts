import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-choose-language',
	templateUrl: './choose-language.page.html',
	styleUrls: ['./choose-language.page.scss'],
})
export class ChooseLanguagePage implements OnInit {

	constructor() { }

	ngOnInit() {
	}

	selectLanguage(language?) {
		if (language == 'english') {
			document.getElementById('english').style.backgroundColor = '#164D59'
			document.getElementById('hindi').style.backgroundColor = '#EAEAEA'
			document.getElementById('kannad').style.backgroundColor = '#EAEAEA'
			document.getElementById('englishimageunselected').setAttribute('src','./assets/images/Magnifier icon_selected 16x16.png');
			document.getElementById('hindiimageunselected').setAttribute('src','./assets/images/Magnifier icon_unselected 16x16.png');
			document.getElementById('kannadimageunselected').setAttribute('src','./assets/images/Magnifier icon_unselected 16x16.png');
		}
		if (language == 'hindi') {
			document.getElementById('english').style.backgroundColor = '#EAEAEA'
			document.getElementById('hindi').style.backgroundColor = '#164D59'
			document.getElementById('kannad').style.backgroundColor = '#EAEAEA'
			document.getElementById('englishimageunselected').setAttribute('src','./assets/images/Magnifier icon_unselected 16x16.png');
			document.getElementById('hindiimageunselected').setAttribute('src','./assets/images/Magnifier icon_selected 16x16.png');
			document.getElementById('kannadimageunselected').setAttribute('src','./assets/images/Magnifier icon_unselected 16x16.png');
		}
		if (language == 'kannad') {
			document.getElementById('english').style.backgroundColor = '#EAEAEA'
			document.getElementById('hindi').style.backgroundColor = '#EAEAEA'
			document.getElementById('kannad').style.backgroundColor = '#164D59'
			document.getElementById('englishimageunselected').setAttribute('src','./assets/images/Magnifier icon_unselected 16x16.png');
			document.getElementById('hindiimageunselected').setAttribute('src','./assets/images/Magnifier icon_unselected 16x16.png');
			document.getElementById('kannadimageunselected').setAttribute('src','./assets/images/Magnifier icon_selected 16x16.png');
		}
	}
}