import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ManageNotificationsPage } from './manage-notifications.page';

describe('ManageNotificationsPage', () => {
  let component: ManageNotificationsPage;
  let fixture: ComponentFixture<ManageNotificationsPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ManageNotificationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
