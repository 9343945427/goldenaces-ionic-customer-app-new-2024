import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Storage } from '@ionic/storage';
import { IonToggle } from '@ionic/angular';
@Component({
	selector: 'app-manage-notifications',
	templateUrl: './manage-notifications.page.html',
	styleUrls: ['./manage-notifications.page.scss'],
})
export class ManageNotificationsPage implements OnInit {
	customer: any;
	errors: any
	@ViewChild('wallet') wallet: ElementRef;
	@ViewChild('promo') promo: ElementRef;
	@ViewChild('whatsapp') whatsapp: ElementRef;
	constructor(
		private storage: Storage,
		public loading: LoadingService,
		public api: ApiService,
	) { }

	ngOnInit() {
		this.storage.get("customer").then((data) => {
			if (data !== null) {
				this.customer = data;
				console.log(this.customer)
			}
		});
	}

	async manageNotification(type, status) {
		let payload = {
			customer_id: this.customer.customer_id,
			notification: type,
			status: status
		}
		await this.loading.open();
		await this.api.httpCall("manageNotification", payload).then(async (response) => {
			this.loading.close();
			await this.storage.set('customer', this.customer);
			this.loading.message('Your preferences have been updated.')
			console.log(response)
		},
			(error) => {
				this.loading.close();
				this.errors = error.error.errors;
			}
		);
	}

	toggle(e, type, status) {
		if (type == 'wallet') {
			e.detail.value == 1 ? e.detail.value = 0 : e.detail.value = e.detail.value
			this.manageNotification(type, status)
		}
		if (type == 'promo') {
			e.detail.value == 1 ? e.detail.value = 0 : e.detail.value = e.detail.value
			this.manageNotification(type, status)
		}
		if (type == 'whatsapp') {
			e.detail.value == 1 ? e.detail.value = 0 : e.detail.value = e.detail.value
			this.manageNotification(type, status)
		}
		console.log(this.customer)
	}
}
