import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManageNotificationsPage } from './manage-notifications.page';

const routes: Routes = [
  {
    path: '',
    component: ManageNotificationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageNotificationsPageRoutingModule {}
