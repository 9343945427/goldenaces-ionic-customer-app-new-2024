import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading.service';
import { Storage } from '@ionic/storage';
import { ApiService } from 'src/app/services/api.service';
import { NavController } from '@ionic/angular';

@Component({
	selector: 'app-qrcode',
	templateUrl: './qrcode.page.html',
	styleUrls: ['./qrcode.page.scss'],
})
export class QrcodePage implements OnInit {
	customer: any
	code: any;
	watchID: any
	logStatus: any;

	constructor(
		private storage: Storage,
		public loading: LoadingService,
		private route: ActivatedRoute,
		public router: Router,
		public api: ApiService,
		private navCtrl: NavController
	) {
		this.route.queryParams.subscribe(params => {
			if (this.router.getCurrentNavigation().extras.state) {
				this.code = this.router.getCurrentNavigation().extras.state['qrcode'];
			}
		});
	}

	ngOnInit() {

	}

	async ionViewWillEnter() {
		await this.storage.get('customer').then((data) => {
			if (data !== null) {
				this.customer = data;
				this.getSatus()
			}
		});
	}

	ionViewWillLeave() {
		clearInterval(this.watchID)
	}

	getSatus() {
		this.watchID = setInterval(() => {
			this.checkLogStatus()
		}, 2000);
	}

	async checkLogStatus() {
		let customer = {
			customer_id: this.customer,
			log_type: 'Customer'
		}
		this.api.httpCall('logStatus', customer).then((response) => {
			this.storage.set('logStatus', response['status'])
			this.storage.set('branch', response['log'].branch)
			this.logStatus = response['status']
			if (this.logStatus != 'CheckIn') {
				clearInterval(this.watchID)
				this.navCtrl.navigateRoot('/home');
				let message = "Thank you for Checking In at Golden Aces Club, " + response['log'].branch.branch_name + '. ' + "Have a great day!"
				this.loading.presentAlert('Check-In', message)
			}
		}, (error) => {
			console.log(error)
		});
	}
}