import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReferralAlertPage } from './referral-alert.page';

const routes: Routes = [
  {
    path: '',
    component: ReferralAlertPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReferralAlertPageRoutingModule {}
