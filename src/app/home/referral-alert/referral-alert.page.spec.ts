import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReferralAlertPage } from './referral-alert.page';

describe('ReferralAlertPage', () => {
  let component: ReferralAlertPage;
  let fixture: ComponentFixture<ReferralAlertPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ReferralAlertPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
