import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Storage } from '@ionic/storage';
@Component({
	selector: 'app-referral-alert',
	templateUrl: './referral-alert.page.html',
	styleUrls: ['./referral-alert.page.scss'],
})
export class ReferralAlertPage implements OnInit {
	errors: any
	reference_types = []
	reference_type_id: any
	payload = {
		customer_id: null,
		reference_type_id: null
	}
	customer: any
	constructor(
		public loading: LoadingService,
		public api: ApiService,
		public modalCtrl: ModalController,
		private navCtrl: NavController,
		private storage: Storage,
	) { }

	ngOnInit() {
	}

	async ionViewWillEnter() {
		await this.storage.get('customer').then((data) => {
			if (data !== null) {
				this.customer = data;
				this.payload.customer_id = this.customer.customer_id
			}
		});
		this.getReferranceTypes()
	}

	close() {
		this.modalCtrl.dismiss()
	}

	async getReferranceTypes() {
		await this.api.httpCall('getReferenceTypes', '').then(response => {
			this.reference_types = response['data']
		}, (error) => {

			this.errors = error.error.errors;
		});
	}

	radio(e) {
		this.reference_type_id = e.detail.value
		this.payload.reference_type_id = this.reference_type_id
	}

	async submit() {
		await this.loading.open()
		await this.api.httpCall('updateCustomerReferenceType', this.payload).then(response => {
			this.loading.close()
			this.storage.set('customer', response['data']);
			this.navCtrl.navigateRoot('/home')
		}, (error) => {
			this.loading.close()
			this.errors = error.error.errors;
		});
	}

	storeTime() {
		let current_time = new Date()
		this.storage.set('current_time', current_time).then(() => {
			this.navCtrl.navigateRoot('/tabs')
		})
	}
}