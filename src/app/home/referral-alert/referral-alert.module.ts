import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReferralAlertPageRoutingModule } from './referral-alert-routing.module';

import { ReferralAlertPage } from './referral-alert.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReferralAlertPageRoutingModule
  ],
  declarations: [ReferralAlertPage]
})
export class ReferralAlertPageModule {}
