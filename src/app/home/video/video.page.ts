import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import youtubePlayer from 'youtube-player';
@Component({
	selector: 'app-video',
	templateUrl: './video.page.html',
	styleUrls: ['./video.page.scss'],
})
export class VideoPage implements OnInit {
	video: any;
	url: any;
	constructor(
		private route: ActivatedRoute,
		public router: Router,
		private dom_sanitizer: DomSanitizer
	) {
		this.route.queryParams.subscribe(params => {
			if (this.router.getCurrentNavigation().extras.state) {
				this.video = this.router.getCurrentNavigation().extras.state['video']
				this.url = this.dom_sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + this.video.youtube_video_id)
			}
		});
	}

	ngOnInit() {
		let player = youtubePlayer('divid')
		player.stopVideo()
		player.loadVideoById(this.video.youtube_video_id).then(() => {
			player.playVideo()
		})
	}

}
