import { Component, OnInit, ViewChild } from '@angular/core';
import Swiper from 'swiper';
import { Scrollbar, SwiperOptions } from 'swiper';
import { Storage } from '@ionic/storage';
import { LoadingService } from '../services/loading.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AlertController, NavController, ModalController, } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import youtubePlayer from 'youtube-player';
import { DomSanitizer } from '@angular/platform-browser';
import { VideoPage } from './video/video.page';
import { BarcodeScanner } from '@capacitor-mlkit/barcode-scanning';
declare const Pusher: any;

@Component({
	selector: 'app-home',
	templateUrl: './home.page.html',
	styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
	public progress = 0.10;
	meta = {
		keyword: "transaction_id",
		order_by: "desc",
		per_page: 5,
		page: 1,
		customer_id: '',
	}
	customer: any;
	sliders: any;
	logStatus: any;
	log: any;
	branch: any;
	reference_type_id: any;
	walletAmount = 0;
	transactions = [];
	errors: any;
	SwiperOptions: SwiperOptions;
	videos: any;
	player: any;
	constructor(
		private storage: Storage,
		public loading: LoadingService,
		public api: ApiService,
		public router: Router,
		public alertController: AlertController,
		private navCtrl: NavController,
		public modalCtrl: ModalController,
		private dom_sanitizer: DomSanitizer
	) {
		this.SwiperOptions = {
			autoplay: {
				delay: 5000,
			},
			loop: true,
			zoom: true
		}
	}

	ngOnInit() {
		this.getSliders();
		var pusher = new Pusher("23336cc5a44f672b9f2c", {
			cluster: "ap2",
		}, err => {
			console.log(err)
		});
		var channel = pusher.subscribe("golden_aces");
		channel.bind("transactions", (data) => {
			if (data.transaction.customer_id == this.customer.customer_id) {
				this.confirmTransactions(data);
			}
		}, err => {
			console.log(err)
		});

	}

	async confirmTransactions(transaction) {
		const alert = await this.alertController.create({
			cssClass: 'alert',
			header: 'Alert',
			backdropDismiss: false,
			message: 'Your transaction for ' + transaction.transaction.transaction_type + ' was ' + transaction.transaction.status + '.',
			buttons: [
				{
					text: 'Ok',
					id: 'confirm-button',
					handler: async () => {
						location.reload();
					}
				}
			]
		});
		await alert.present();
	}

	async ionViewWillEnter() {
		await this.storage.get("customer").then((data) => {
			if (data !== null) {
				this.customer = data;
				this.reference_type_id = data.reference_type_id;
				this.meta.customer_id = data.customer_id;
				this.checkLogStatus();
			}
		});
		await BarcodeScanner.isGoogleBarcodeScannerModuleAvailable().then(res => {
			if (res.available == false) BarcodeScanner.installGoogleBarcodeScannerModule()
		})
	}

	async requestPermissions(): Promise<boolean> {
		const { camera } = await BarcodeScanner.requestPermissions();
		return camera === 'granted' || camera === 'limited';
	}

	async presentPermissionAlert(): Promise<void> {
		const alert = await this.alertController.create({
			header: 'Permission denied',
			message: 'Please grant Camera permissions to use the barcode scanner.',
			buttons: ['Settings'],
		});
		await alert.present();
		alert.onDidDismiss().then(() => {
			BarcodeScanner.openSettings()
		})
	}
	async getSliders() {
		await this.loading.open();
		await this.api.httpCall("getSliders", "").then(
			async (response) => {
				this.loading.close();
				this.sliders = response["data"];
				this.getVideos()
			},
			(error) => {
				this.loading.close();
				this.errors = error.error.errors;
			}
		);
	}

	async getVideos() {
		await this.loading.open();
		await this.api.httpCall("getVideos", '').then((response) => {
			this.loading.close();
			this.videos = response['data']
			this.referralAlert();
		},
			(error) => {
				this.loading.close();
				this.errors = error.error.errors;
				this.referralAlert();
			}
		);
	}

	getUrl(video) {
		return this.dom_sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + video.youtube_video_id)
	}

	async playVideo(video) {
		let navigationExtras: NavigationExtras = {
			state: {
				video: video,
			}
		};
		this.router.navigate(['/home/video'], navigationExtras);
		// var windowReference = window.open();
		// windowReference.location = 'https://www.youtube.com/embed/' + video.youtube_video_id
	}

	async referralAlert() {
		let hours = null;
		await this.storage.get("current_time").then((data) => {
			if (data != null) {
				let current_time = data;
				let new_time = new Date();
				hours = Math.abs(new_time.getTime() - current_time.getTime()) / 36e5;
			}
		});
		if (this.reference_type_id == null) {
			if (hours > 1 || hours == null) {
				this.navCtrl.navigateRoot(["/home/referral-alert"]);
			}
		}
	}

	async checkLogStatus(event?) {
		let payload = {
			customer_id: this.customer.customer_id,
			log_type: 'Customer'
		};
		this.api.httpCall("logStatus", payload).then(
			(response) => {
				this.logStatus = response["status"];
				this.log = response["log"];
				this.branch = response["log"].branch;
				this.storage.set('branch', this.branch)
				if (event) {
					event.target.complete();
				}
				this.getCustomerWallet();
			},
			(error) => {
				this.errors = error.error.errors;
			}
		);
	}

	async getCustomerWallet(event?) {
		await this.api.httpCall("transactions/getCustomerWallet", this.customer).then(
			(response) => {
				this.walletAmount = response["opening_balance"];
				if (event) {
					event.target.complete();
				}
			}, (error) => {
				this.errors = error.error.errors;
			}
		);
	}

	async generateQRCode() {
		//Without KYC Status
		let payload = {
			customer_id: this.customer.customer_id,
			log_type: 'Customer'
		};
		await this.loading.open();
		await this.api.httpCall("generateQRCode", payload).then(
			(response) => {
				this.loading.close();
				let navigationExtras: NavigationExtras = {
					state: {
						qrcode: response["data"].code,
					},
				};
				this.router.navigate(["/home/qrcode"], navigationExtras);
			},
			(error) => {
				this.loading.close();
				this.presentAlert("Account Disabled", error.error.message[0]);
				this.errors = error.error.errors;
			}
		);
	}

	async scanSeat() {
		const { barcodes } = await BarcodeScanner.scan();
		let payload = {
			seat_code: barcodes[0].rawValue,
			customer_id: this.customer.customer_id,
			branch_id: this.branch.branch_id
		};
		await this.loading.open();
		await this.api.httpCall("occupySeat", payload).then(
			(response) => {
				this.loading.close();
				console.log(response)
				this.loading.presentAlert("Success", 'You are seated successfully.')
			},
			(error) => {
				this.loading.close();
				this.errors = error.error.errors;
				console.log(error)
				this.loading.presentAlert('Seating', error.message)
			}
		);
	}

	async checkOut() {
		let payload = {
			log_id: this.log.log_id,
		};
		await this.loading.open();
		await this.api.httpCall("checkOut", payload).then(
			(response) => {
				this.loading.close();
				this.logStatus = "CheckIn";
				this.log = null;
				this.branch = null;
				this.loading.presentAlert("Check-Out", "Thank you for your time at Golden Aces Club. We hope to see you again!");
			},
			(error) => {
				this.loading.close();
				this.errors = error.error.errors;
			}
		);
	}

	async checkOutConfirm() {
		const alert = await this.alertController.create({
			cssClass: 'alert',
			header: 'Confirm !',
			message: 'Are you sure ?',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'alert',
					id: 'cancel-button',
					handler: () => {

					}
				}, {
					text: 'Check-Out',
					id: 'confirm-button',
					handler: () => {
						this.checkOut();
					}
				}
			]
		});
		await alert.present();
	}

	cashTables() {
		//Check KYC Status
		// if (this.customer.kyc_status == 0) {
		//     this.presentAlert("KYC is not updated", "Please visit Account page to complete your KYC.").then(() => {
		//         this.router.navigate(["/tabs/account"]);
		//     });
		// }
		// else if (this.logStatus == "CheckIn") {
		//     this.loading.presentAlert("Alert", "Please Check-In before any transactions.");
		// }
		// else {
		//     this.router.navigate(["/tabs/home/cash-tables"]);
		// }

		//No checking KYC Status

		if (this.logStatus == "CheckIn") {
			this.loading.presentAlert("Alert", "Please Check-In before any transactions.");
		}
		else {
			this.router.navigate(["/tabs/home/cash-tables"]);
		}
	}

	wallet() {
		//Check kyc status
		// if (this.customer.kyc_status == 0) {
		//     this.presentAlert("KYC is not updated", "Please visit Account page to complete your KYC.").then(() => {
		//         this.router.navigate(["/tabs/account"]);
		//     });
		// }
		// else {
		//     this.router.navigate(["/tabs/home/transactions"]);
		// }

		//dont check kyc status
		this.router.navigate(["/wallet"]);
	}

	tournaments() {
		//check kyc status
		// if (this.customer.kyc_status == 0) {
		//     this.presentAlert("KYC is not updated", "Please visit Account page to complete your KYC.").then(() => {
		//         this.router.navigate(["/tabs/account"]);
		//     });
		// }
		// else {
		//     this.router.navigate(["/tabs/home/tournaments"]);
		// }

		//Dont check kyc status
		this.router.navigate(["/tabs/home/tournaments"]);
	}

	async presentAlert(title, message) {
		const alert = await this.alertController.create({
			cssClass: "alert",
			header: title,
			message: message,
			buttons: ["OK"],
		});
		alert.onDidDismiss().then(() => {
			this.router.navigate(["/profile"]);
		});
		await alert.present();
	}
}