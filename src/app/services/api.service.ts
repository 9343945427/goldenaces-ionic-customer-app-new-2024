import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { LoadingService } from './loading.service';
import { AngularFireMessaging } from '@angular/fire/compat/messaging';
import { tap } from 'rxjs/operators'
@Injectable({
	providedIn: 'root'
})
export class ApiService {
	url = environment.apiUrl;
	token: any;

	constructor(
		private http: HttpClient,
		private storage: Storage,
		private navCtrl: NavController,
		public loading: LoadingService,
		private afMessaging: AngularFireMessaging
	) {

	}

	open(uri, data) {
		return new Promise((resolve, reject) => {
			this.http.post(this.url + uri, data)
				.subscribe(response => {
					resolve(response);
				}, (error) => {
					reject(error);
				});
		});
	}

	async httpCall(uri, data) {
		await this.storage.get('token').then((data) => {
			if (data != null) {
				this.token = data;
			}
		});
		return new Promise((resolve, reject) => {
			this.http.post(this.url + uri, data, {
				headers: new HttpHeaders().set('Authorization', 'Bearer' + ' ' + this.token)
			}).subscribe(response => {
				resolve(response);
			}, (error) => {
				reject(error);
				if (error.error.message == 'Unauthenticated.') {
					this.loading.presentAlert("Unauthenticated", "Dear customer you were logged off, Please login.")
					this.storage.remove('customer');
					this.storage.remove('token');
					this.storage.remove('code')
					this.storage.remove('user')
					this.storage.remove('logStatus')
					this.storage.remove('branch')
					this.navCtrl.navigateRoot('/signup');
				}
			});
		});
	}

	requestPermission() {
		return this.afMessaging.requestToken.pipe(
			tap(token => {
				console.log('Store token to server: ', token);
			}, error => {
				console.log(error)
			})
		);
	}

	getMessages() {
		return this.afMessaging.messages;
	}

	logout() {
		this.storage.remove('customer');
		this.storage.remove('token');
		this.navCtrl.navigateRoot('/login');
	}
}