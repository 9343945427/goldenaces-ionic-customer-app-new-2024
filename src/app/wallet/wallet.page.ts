import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ApiService } from '../services/api.service';
import { LoadingService } from '../services/loading.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { IonModal } from '@ionic/angular';
import { Gesture, GestureController } from '@ionic/angular';
import { PhonePePaymentPlugin } from 'phonepe-payment-capacitor'
import { Renderer2 } from '@angular/core';
import { Capacitor } from '@capacitor/core';
@Component({
	selector: 'app-wallet',
	templateUrl: './wallet.page.html',
	styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {
	meta = {
		keyword: "transaction_id",
		order_by: "desc",
		per_page: 5,
		customer_id: '',
		branch_id: '',
		transaction_id: '',
		transaction_type: '',
		otp: null,
		amount: null,
		status: 'Pending',
		tables: [1],
		seat_no: 1,
		table_id: '1',
		table_log_id: '1',
		tds: null,
		earnings: null
	}

	others = false;
	customer: any
	transactions: any;
	walletAmount = null;
	errors: any;
	watchID: any;
	isOpen: any;
	withdraw_amount: any;
	terms: any;
	logStatus: any;
	@ViewChild(IonModal) modal: IonModal;
	@ViewChild('withdraw_modal') withdraw_modal: IonModal;
	@ViewChild('deposit_modal') deposit_modal: IonModal;
	@ViewChild('buy_in_modal') buy_in_modal: IonModal;
	@ViewChild('cash_out_modal') cash_out_modal: IonModal;
	@ViewChild('swipe') swipe: ElementRef;
	@ViewChild('swipetext') swipetext: ElementRef;
	constructor(
		public loading: LoadingService,
		public api: ApiService,
		public router: Router,
		private storage: Storage,
		private navCtrl: NavController,
		private el: ElementRef,
		private gestureCtrl: GestureController,
		public renderer: Renderer2,
		private platform: Platform,
		public alertController: AlertController,
	) { }

	async ngOnInit() {
	}

	ionViewWillEnter() {
		this.storage.get('customer').then((data) => {
			if (data != null) {
				if (data != null) {
					this.customer = data;
					this.meta.customer_id = data.customer_id
					this.meta.amount = null
					this.getCustomerWallet()
					this.storage.get('branch').then((data) => {
						if (data != null) {
							this.meta.branch_id = data.branch_id;
						}
					});
				}
			}
		});
	}

	async getCustomerWallet(event?) {
		await this.loading.open();
		await this.api.httpCall('transactions/getCustomerWallet', this.meta).then((response) => {
			this.loading.close();
			this.walletAmount = response['opening_balance']
			this.withdraw_amount = response['withdraw_amount']
			this.meta.amount = null
			this.getTransactions();
			if (event) {
				event.target.complete();
			}
		}, (error) => {
			if (error) {
				this.errors = error.error.errors;
				this.loading.close()
			}
		});
	}

	async getTransactions() {
		await this.api.httpCall('transactions/customerTransactions', this.meta).then((response) => {
			this.transactions = response['data'];
		}, (error) => {
			this.errors = error.error.errors;
		});
	}

	async calculateTds() {
		if (this.walletAmount == 0) {
			alert('Insufficeint balance in your wallet.')
		} else {
			await this.loading.open();
			await this.api.httpCall('transactions/tds', this.meta).then((response) => {
				this.loading.close();
				this.meta.tds = response;
				this.meta.earnings = this.meta.amount - this.meta.tds
				if (this.meta.earnings < 0) {
					this.meta.earnings = 0
				}
			}, (error) => {
				this.errors = error.error.errors;
				this.loading.close()
			});
		}
	}

	async confirmWithdrawal() {
		const alert = await this.alertController.create({
			cssClass: 'alert',
			header: 'Confirm !',
			message: 'Are you sure ?',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'alert',
					id: 'cancel-button',
					handler: () => {

					}
				}, {
					text: 'Withdraw',
					id: 'confirm-button',
					handler: () => {
						this.withdrawal();
					}
				}
			]
		});
		await alert.present();
	}

	async withdrawal() {
		let payload = {
			amount: this.meta.amount,
			tds: this.meta.tds,
			customer_id: this.meta.customer_id,
			transaction_type: 'Withdraw',
			total_amount: this.meta.earnings,
			branch_id: this.meta.branch_id,
		}
		await this.loading.open();
		if (this.meta.amount > this.withdraw_amount) {
			this.loading.presentAlert('Transaction Failed', "InSufficient Withdrawable Balance!")
			this.loading.close()
		}
		else {
			await this.api.httpCall('transactions/withdraw', payload).then((res) => {
				this.loading.close();
				this.loading.presentAlert('Success', "Your request for Withdrawal was received.Please wait for Admin to approve.");
				this.withdraw_modal.dismiss()
				this.getCustomerWallet()
			}, (error) => {
				this.loading.close()
				this.errors = error.error.errors;
				this.loading.presentAlert('Message', error.error.message)
			});
		}
	}

	onChange(e) {
		this.meta.tds = null
		this.meta.earnings = null
	}

	async checkLogStatus() {
		let payload = {
			customer_id: this.customer.customer_id,
			log_type: 'Customer'
		};
		await this.api.httpCall("logStatus", payload).then(
			(response) => {
				this.logStatus = response["status"];
				this.storage.set('branch', response["log"].branch)
			},
			(error) => {
				this.errors = error.error.errors;
			}
		);
	}


	doRefresh(e) {
		this.getCustomerWallet(e)
	}

	depositGesture() {
		setTimeout(() => {
			const gesture: Gesture = this.gestureCtrl.create({
				el: this.swipe.nativeElement,
				threshold: 0,
				gestureName: 'swipe',
				direction: 'x',
				onStart: (detail) => {
					let velocity = Math.sign(detail.velocityX)
					this.renderer.setStyle(this.swipe.nativeElement, "transition", "none")
					if (velocity == 0 || velocity == -1 && detail.currentX == 0) this.renderer.setStyle(this.swipe.nativeElement, "transition", "none")
				},
				onMove: (detail) => {
					let velocity = Math.sign(detail.velocityX)
					if (velocity == 0 || velocity == -1 && detail.currentX == 0) this.renderer.setStyle(this.swipe.nativeElement, "transition", "none")
					this.renderer.setStyle(this.swipe.nativeElement, "transform", `translateX(${detail.deltaX}px)`)
					if (detail.currentX > 125 && detail.currentX < 220 && detail.velocityX >= 0 && velocity == 1) {
						this.swipetext.nativeElement.style.opacity = '0.1';
					}
					if (velocity == -1) {
						setTimeout(() => {
							this.swipetext.nativeElement.style.opacity = '0.1';
						}, 100);
						setTimeout(() => {
							this.swipetext.nativeElement.style.opacity = '0.2';
						}, 200);
						setTimeout(() => {
							this.swipetext.nativeElement.style.opacity = '0.3';
						}, 300);
						setTimeout(() => {
							this.swipetext.nativeElement.style.opacity = '0.4';
						}, 400);
						setTimeout(() => {
							this.swipetext.nativeElement.style.opacity = '0.5';
						}, 500);
						setTimeout(() => {
							this.swipetext.nativeElement.style.opacity = '0.6';
						}, 600);
						setTimeout(() => {
							this.swipetext.nativeElement.style.opacity = '0.7';
						}, 700);
						setTimeout(() => {
							this.swipetext.nativeElement.style.opacity = '0.8';
						}, 800);
						setTimeout(() => {
							this.swipetext.nativeElement.style.opacity = '0.9';
						}, 900);
						setTimeout(() => {
							this.swipetext.nativeElement.style.opacity = '1';
						}, 999);
					}
					if (detail.currentX >= 328 && velocity == 1 && detail.deltaX != 0) {
						this.renderer.setStyle(this.swipe.nativeElement, "transition", `0.4s ease-out`)
						this.renderer.setStyle(this.swipe.nativeElement, "transform", `translateX(${0}px)`)
					}
				},
				onEnd: (detail) => {
					let velocity = Math.sign(detail.velocityX)
					this.renderer.setStyle(this.swipe.nativeElement, "transition", `0.4s ease-out`)
					this.renderer.setStyle(this.swipe.nativeElement, "transform", `translateX(${0}px)`)
					this.swipetext.nativeElement.style.opacity = '0.1';
					this.swipetext.nativeElement.style.opacity = '0.5';
					this.swipetext.nativeElement.style.opacity = '1';
					if (detail.currentX >= 328 && velocity == 1 && detail.deltaX != 0) {
						// this.deposit()
						this.phonePe()
						gesture.destroy()
					}
				},
			}, true);
			gesture.enable();
		}, 500);
	}

	selectBuyIn() {
		document.getElementById('Buy-In').setAttribute('class', 'deposit-button md button button-block button-solid ion-activatable ion-focusable hydrated')
	}

	selectCashOut() {
		document.getElementById('Cash-Out').setAttribute('class', 'deposit-button md button button-block button-solid ion-activatable ion-focusable hydrated')

	}

	selectWithdraw() {
		document.getElementById('withdraw').setAttribute('class', 'deposit-button md button button-block button-solid ion-activatable ion-focusable hydrated')
	}

	async deposit() {
		this.meta.transaction_type = 'Deposit';
		await this.loading.open();
		await this.api.httpCall('transactions/depositAmount', this.meta).then((res) => {
			this.loading.close();
			this.loading.presentAlert('Success', "Your request for Deposit was received. Please wait for Cashier to approve.");
			this.modal.dismiss()
		}, (error) => {
			this.loading.close()
			this.errors = error.error.errors;
			this.modal.dismiss()
			this.loading.presentAlert('Message', error.error.message)
		});
	}

	async buyIn() {
		this.meta.transaction_type = 'BuyIn',
			await this.loading.open();
		if (this.meta.amount > this.walletAmount) {
			this.loading.presentAlert('Transaction Failed', "InSufficient balance in your wallet")
			this.loading.close()
		}
		else {
			await this.api.httpCall('transactions/buyIn', this.meta).then((response) => {
				this.loading.close();
				this.loading.presentAlert('Success', "Your request for Buy-In was received. Please wait for Cashier to approve.");
				this.closeBuyIn();
			}, (error) => {
				this.errors = error.error.errors;
				this.loading.presentAlert('Message', error.error.message)
				this.loading.close()
				this.closeBuyIn();
			});
		}
	}

	async cashOut() {
		this.meta.transaction_type = 'CashOut'
		await this.loading.open();
		await this.api.httpCall('transactions/cashOut', this.meta).then((response) => {
			this.loading.close();
			this.loading.presentAlert('Success', "Your request for Cash-Out was received.Please wait for cashier to approve.");
			this.closeCashOut()
		}, (error) => {
			this.loading.close()
			this.errors = error.error.errors;
			this.loading.presentAlert('Message', error.error.message)
			this.closeCashOut()
		});
	}

	getSatus(transaction_id) {
		this.watchID = setInterval(() => {
			this.showTransaction(transaction_id)
		}, 2000);
	}

	async showTransaction(transaction_id) {
		let data = {
			transaction_id: transaction_id
		}
		await this.api.httpCall('transactions/showTransaction', data).then((response) => {
			let status = response['data'].status
			if (status == 'Approved') {
				clearInterval(this.watchID);
				this.meta.transaction_id = null
				this.loading.presentAlert('Success', "Your transaction for Deposit was Approved.");
				this.navCtrl.navigateRoot(['/tabs/home']);
			}
			if (status == 'Rejected') {
				clearInterval(this.watchID);
				this.meta.transaction_id = null
				this.loading.presentAlert('Failed', "Your transaction for Deposit was Rejected.");
				this.navCtrl.navigateRoot(['/tabs/home']);
			}
		}, (error) => {
			this.errors = error.error.errors;
		});
	}

	showOthers() {
		this.others == false ? this.others = true : this.others = false;
		document.getElementById('1000').style.background = '#ffff'
		document.getElementById('2500').style.background = '#ffff'
		document.getElementById('5000').style.background = '#ffff'
		this.meta.amount = '10000';
	}

	selectDepositAmount(amount) {
		this.others = false
		this.meta.amount = amount;
		if (amount == 1000) {
			document.getElementById('1000').style.background = '#D3EAEB'
			document.getElementById('2500').style.background = '#ffff'
			document.getElementById('5000').style.background = '#ffff'
		}
		if (amount == 2500) {
			document.getElementById('2500').style.background = '#D3EAEB'
			document.getElementById('1000').style.background = '#ffff'
			document.getElementById('5000').style.background = '#ffff'
		}
		if (amount == 5000) {
			document.getElementById('5000').style.background = '#D3EAEB'
			document.getElementById('2500').style.background = '#ffff'
			document.getElementById('1000').style.background = '#ffff'
		}
	}

	selectBuyinAmount(amount) {
		this.others = false
		this.meta.amount = amount;
		if (amount == 1000) {
			document.getElementById('1000').style.background = '#D3EAEB'
			document.getElementById('2500').style.background = '#ffff'
			document.getElementById('5000').style.background = '#ffff'
		}
		if (amount == 2500) {
			document.getElementById('2500').style.background = '#D3EAEB'
			document.getElementById('1000').style.background = '#ffff'
			document.getElementById('5000').style.background = '#ffff'
		}
		if (amount == 5000) {
			document.getElementById('5000').style.background = '#D3EAEB'
			document.getElementById('2500').style.background = '#ffff'
			document.getElementById('1000').style.background = '#ffff'
		}
	}

	selectCashOutAmount(amount) {
		this.others = false
		this.meta.amount = amount;
		if (amount == 1000) {
			document.getElementById('1000').style.background = '#D3EAEB'
			document.getElementById('2500').style.background = '#ffff'
			document.getElementById('5000').style.background = '#ffff'
		}
		if (amount == 2500) {
			document.getElementById('2500').style.background = '#D3EAEB'
			document.getElementById('1000').style.background = '#ffff'
			document.getElementById('5000').style.background = '#ffff'
		}
		if (amount == 5000) {
			document.getElementById('5000').style.background = '#D3EAEB'
			document.getElementById('2500').style.background = '#ffff'
			document.getElementById('1000').style.background = '#ffff'
		}
	}

	dismissModal(e) {
		this.meta.tds = null
		this.meta.earnings = null
		this.meta.amount = null
		this.others = false;
		this.errors = undefined
	}

	closeBuyIn() {
		this.buy_in_modal.dismiss().then(() => {
			this.others = false;
			this.meta.amount = null;
		})
	}

	closeCashOut() {
		this.cash_out_modal.dismiss().then(() => {
			this.others = false;
			this.meta.amount = null
		})
	}

	closeWithdraw() {
		this.withdraw_modal.dismiss().then(() => {
			this.others = false;
			this.meta.amount = null
		})
	}

	changeFormat(date_time) {
		return moment(date_time).format('DD-MM-YYYY : hh:mm a')
	}

	async phonePe() {
		if (Capacitor.getPlatform() == 'web') {
			this.makePayment()
		} else {
			PhonePePaymentPlugin.init({
				environment: 'PRODUCTION',
				merchantId: 'M1PYS05HJTVS',
				appId: null,
				enableLogging: true
			}).then(result => {
				console.log('VS: ' + JSON.stringify(result['status']));
				this.initPayment()
			}).catch(error => {
				console.log('VS: error:' + error.message);
			})
		}
	}

	async makePayment() {
		await this.loading.open();
		await this.api.httpCall('phonepe/makePayment', this.meta).then((response) => {
			this.loading.close();
			var windowReference = window.open();
			windowReference.location = response['url']
		}, (error) => {
			this.loading.close()
			this.errors = error.error.errors;
			this.loading.presentAlert('Message', error.error.message)
		});
	}

	async initPayment() {
		await this.loading.open();
		await this.api.httpCall('phonepe/initPayment', this.meta).then((response) => {
			this.loading.close();
			this.startTransaction(response['body'], response['checksum'])
		}, (error) => {
			this.loading.close()
			this.errors = error.error.errors;
			this.loading.presentAlert('Message', error.error.message)
		});
	}

	async startTransaction(body, checksum) {
		await PhonePePaymentPlugin.startTransaction({
			body: body,
			checksum: checksum,
			packageName: 'club.goldenaces.app', // Package name
			appSchema: "Golden Aces",
		}).then((res) => {
			if (res['status'] == 'FAILURE') {
				let error = res['error']
				let string = error
				let x = string.split('key_error_code:ERROR_B2B_API_RETURNED_ERRORkey_error_result:')
				alert(JSON.parse(x[1]).message)
			} else {
				this.deposit_modal.dismiss();
				this.getCustomerWallet()
				this.loading.message('Deposit successfull. Amount is added to your wallet.');
				console.log("VS: " + JSON.stringify(res));
			}
		}).catch((error) => {
			console.log("VS: error:" + error.message);
		});
	}

	async viewTransaction(transaction) {
		let navigationExtras: NavigationExtras = {
			state: {
				transaction: transaction,
				page: 'wallet'
			},
		};
		this.router.navigate(["/transactions/view-transactions"], navigationExtras);
	}
}