import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { NavigationExtras, Router, } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { ApiService } from '../services/api.service';
import { Storage } from '@ionic/storage';
import { ModalController, NavController } from '@ionic/angular';
import { PoliciesPage } from '../policies/policies.page';
@Component({
	selector: 'app-signup',
	templateUrl: './signup.page.html',
	styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
	//for login
	@ViewChild('checkbox') checkbox: ElementRef;
	loginForm: FormGroup;
	errors: any;
	status = 'true'
	show_login = true;
	user: any

	//for OTP
	show_otp = false;
	otpForm: FormGroup;
	kycStatus: any;
	seconds = 0;
	seconds_id: any;
	resend = false;

	//for referral
	show_referral = false;
	referralForm: FormGroup;
	isModalOpen = false;
	canDismiss = false;
	terms: any;
	click_status = 'true'

	validations = {
		'mobile_no': [
			{ type: 'required', message: 'Mobile number field is required.' },
			{ type: 'pattern', message: 'Enter a valid mobile number.' },
		],
		'otp': [
			{ type: 'required', message: 'OTP field is required.' }
		],
		'email': [
			{ type: 'required', message: 'Email address field is required.' },
			{ type: 'pattern', message: 'Enter a valid email address.' },
		],
	};
	constructor(
		public loading: LoadingService,
		public api: ApiService,
		public router: Router,
		private storage: Storage,
		private modalCtrl: ModalController,
		private navCtrl: NavController
	) {
		this.loginForm = new FormGroup({
			'mobile_no': new FormControl('', Validators.compose([
				Validators.required,
				Validators.pattern('^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$'),
			])),
		});
		this.otpForm = new FormGroup({
			'mobile_no': new FormControl(this.loginForm.value.mobile_no, Validators.compose([])),
			'otp': new FormControl('', Validators.compose([
				Validators.required,
			]))
		});
		this.referralForm = new FormGroup({
			'mobile_no': new FormControl(this.loginForm.value.mobile_no, Validators.compose([])),
			'email': new FormControl('', Validators.compose([
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			])),
			'referral_code': new FormControl('', Validators.compose([])),
		});
	}

	ngOnInit() {

	}

	ionViewWillEnter() {
		this.status = 'true'
	}

	goToLogin() {
		this.status = 'true'
		this.show_login = true;
		this.show_otp = false;
		this.seconds = 30;
		clearInterval(this.seconds_id)
	}

	async viewPolicies(policy) {
		const modal = this.modalCtrl.create({
			component: PoliciesPage,
			componentProps: {
				policy: policy
			}
		})
			; (await modal).present()
	}

	onChange(event?) {
		event.target.checked == true ? this.status = 'true' : this.status = 'false'
	}

	async sendOtp() {
		await this.loading.open();
		await this.api.open('sendOtp', this.loginForm.value).then((response) => {
			this.loading.close();
			this.user = this.loginForm.value.mobile_no
			this.loading.message('OTP sent successfully to your Mobile number.')
			this.show_otp = true
			this.show_login = false
			this.seconds = 30
			this.seconds_id = setInterval(() => {
				this.seconds--
				if (this.seconds == 0) {
					this.resend = true;
					clearInterval(this.seconds_id)
				} else {
					this.resend = false
				}
			}, 1000)
		}, (error) => {
			this.loading.close();
			this.errors = error.error.errors;
		});
	}

	async verifyOtp() {
		let payload = {
			mobile_no: this.loginForm.value.mobile_no,
			otp: this.otpForm.value.otp
		}
		await this.loading.open();
		await this.api.open('verifyOtp ', payload).then(async (response) => {
			this.loading.close();
			this.show_login = false
			this.show_otp = false
			this.kycStatus = response['kyc_status']
			this.storage.set('customer', response['customer']);
			this.storage.set('token', response['token']);
			await this.storage.get('fcm_token').then((data) => {
				if (data != null)
					this.sendToken(data, response['customer'].customer_id)
			})
			this.seconds = 30;
			clearInterval(this.seconds_id)
			this.navCtrl.navigateRoot(['/home']);
		}, (error) => {
			this.errors = error.error.errors;
			this.loading.close();
			this.otpForm.controls["otp"].setValue(null);
			if (error.error.message == 'Please Register') {
				this.loading.message(error.error.message)
				this.seconds = 30;
				clearInterval(this.seconds_id)
				this.show_login = false
				this.show_otp = false
				this.show_referral = true
				this.click_status = 'true'
				this.getLoginTerms();
			}
			else {
				this.loading.presentAlert('Message', error.error.message)
			}
		});
	}

	async sendToken(fcm_token, customer_id) {
		await this.api.httpCall('updateFCMToken', { 'fcm_token': fcm_token, 'customer_id': customer_id }).then((response: any) => {
			console.log(fcm_token)
		})
	}

	async resendOtp() {
		let meta = {
			mobile_no: this.loginForm.value.mobile_no,
		}
		await this.loading.open();
		await this.api.open('sendOtp', meta).then((response) => {
			this.loading.close();
			this.loading.presentAlert('Message', 'OTP sent successfully to your Mobile number.');
			this.otpForm.controls["otp"].setValue(null);
			this.seconds = 30
			this.seconds_id = setInterval(() => {
				this.seconds--
				if (this.seconds == 0) {
					this.resend = true;
					clearInterval(this.seconds_id)
				} else {
					this.resend = false
				}
			}, 1000)
		}, (error) => {
			this.loading.close();
			this.errors = error.error.errors;
			// this.loading.presentAlert('Message', error.error.message);
		});
	}

	async getLoginTerms() {
		await this.api.open('getLoginTerm', '').then((response) => {
			this.terms = response['data']
			this.isModalOpen = true
		}, (error) => {
			this.errors = error.error.errors;
		});
	}

	async getLoginTerm() {
		await this.api.open('getLoginTerm', '').then((response) => {
			this.terms = response['data']
			this.isModalOpen = true
		}, (error) => {
			this.errors = error.error.errors;
		});
	}

	onChanged(event?) {
		event.target.checked == true ? this.click_status = 'true' : this.click_status = 'false'
		event.target.checked == false ? this.isModalOpen = false : this.isModalOpen = true
		console.log(event.target.checked, this.click_status)
	}

	onWillDismiss(e) {
		if (this.click_status == 'true') {
			this.isModalOpen = true
		}
	}

	async register() {
		console.log(this.referralForm.value, this.loginForm.value)
		let payload = {
			mobile_no: this.loginForm.value.mobile_no,
			email: this.referralForm.value.email,
			referral_code: this.referralForm.value.referral_code
		}
		this.seconds = 30;
		clearInterval(this.seconds_id)
		await this.loading.open();
		await this.api.open('registerCustomer', payload).then(async (response) => {
			this.loading.close()
			await this.storage.set('customer', response['customer']);
			await this.storage.set('token', response['token']);
			this.show_referral = false
			this.navCtrl.navigateRoot(['/home']);
		}, (error) => {
			this.errors = error.error.errors;
			this.loading.close();
		});
	}
}
