import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { ApiService } from '../services/api.service';
import { Storage } from '@ionic/storage';
import { AlertController, NavController } from '@ionic/angular';


@Component({
	selector: 'app-otp',
	templateUrl: './otp.page.html',
	styleUrls: ['./otp.page.scss'],
})
export class OtpPage implements OnInit {

	otpForm: FormGroup;
	user: any;
	kycStatus: any;
	errors: any;
	seconds: 0;
	seconds_id: any;
	resend = false;
	validations = {
		'mobile_no': [
			{ type: 'required', message: 'Mobile number field is required.' },
			{ type: 'pattern', message: 'Enter a valid mobile number.' },
		],
		'otp': [
			{ type: 'required', message: 'OTP field is required.' }
		]
	};

	constructor(
		public loading: LoadingService,
		public api: ApiService,
		private route: ActivatedRoute,
		public router: Router,
		private storage: Storage,
		public alertController: AlertController,
		private navCtrl: NavController
	) {
		this.otpForm = new FormGroup({
			'mobile_no': new FormControl('', Validators.compose([
				Validators.required,
				Validators.pattern('^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$'),
			])),
			'otp': new FormControl('', Validators.compose([
				Validators.required,
			]))
		});
		this.route.queryParams.subscribe(params => {
			if (this.router.getCurrentNavigation().extras.state) {
				this.user = this.router.getCurrentNavigation().extras.state['user'];
				this.otpForm.controls["mobile_no"].setValue(this.user.mobile_no);
			}
		});
	}

	ngOnInit() {
		this.seconds = 0
		this.seconds_id = setInterval(() => {
			this.seconds++
			console.log(this.seconds)
			if (this.seconds == 60) {
				this.resend = true;
				clearInterval(this.seconds_id)
			} else {
				this.resend = false
			}
		}, 1000)
	}

	ionViewWillLeave() {
		this.seconds = 0;
		clearInterval(this.seconds_id)
	}
	
	home() {
		this.navCtrl.navigateRoot(['/home']);
	}

	async verifyOtp() {
		await this.loading.open();
		await this.api.open('verifyOtp ', this.otpForm.value).then(async (response) => {
			this.loading.close();
			this.kycStatus = response['kyc_status']
			this.storage.set('customer', response['customer']);
			this.storage.set('token', response['token']);
			await this.storage.get('fcm_token').then((data) => {
				if (data != null)
					this.sendToken(data, response['customer'].customer_id)
			})
			this.navCtrl.navigateRoot(['/home']);
		}, (error) => {
			this.errors = error.error.errors;
			this.loading.close();
			this.otpForm.controls["otp"].setValue(null);
			if (error.error.message == 'Please Register') {
				this.loading.message(error.error.message)
				let navigationExtras: NavigationExtras = {
					state: {
						mobile_no: this.user.mobile_no,
					}
				};
				this.router.navigate(['/email'], navigationExtras);
			}
			else {
				this.loading.presentAlert('Message', error.error.message)
			}
		});
	}

	async sendToken(fcm_token, customer_id) {
		await this.api.httpCall('updateFCMToken', { 'fcm_token': fcm_token, 'customer_id': customer_id }).then((response: any) => {
			console.log(fcm_token)
		})
	}

	async resendOtp() {
		let meta = {
			mobile_no: this.user.mobile_no,
		}
		await this.loading.open();
		await this.api.open('sendOtp', meta).then((response) => {
			this.loading.close();
			this.loading.presentAlert('Message', 'OTP sent successfully to your Mobile number.');
			this.otpForm.controls["otp"].setValue(null);
			this.seconds = 0
			this.seconds_id = setInterval(() => {
				this.seconds++
				if (this.seconds == 60) {
					this.resend = true;
					clearInterval(this.seconds_id)
				} else {
					this.resend = false
				}
			}, 1000)
		}, (error) => {
			this.loading.close();
			this.errors = error.error.errors;
			// this.loading.presentAlert('Message', error.error.message);
		});
	}

}
