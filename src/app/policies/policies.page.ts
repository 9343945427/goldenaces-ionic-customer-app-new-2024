import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'app-policies',
	templateUrl: './policies.page.html',
	styleUrls: ['./policies.page.scss'],
})
export class PoliciesPage implements OnInit {
	@Input() policy: any
	constructor(
		private modalCtrl: ModalController,
		private route: ActivatedRoute,
		public router: Router,
	) {
		
	}

	ngOnInit() {
	}

	close() {
		this.modalCtrl.dismiss()
	}

}
