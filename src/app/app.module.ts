import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
// import { IonicStorageModule } from '@ionic/storage-angular';
import { Storage } from '@ionic/storage';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireMessagingModule } from '@angular/fire/compat/messaging';
import { environment } from 'src/environments/environment';
import { ServiceWorkerModule } from '@angular/service-worker';
@NgModule({
	declarations: [AppComponent],
	imports: [BrowserModule,
		IonicModule.forRoot({
			innerHTMLTemplatesEnabled: true
		}),
		AppRoutingModule,
		HttpClientModule,
		ServiceWorkerModule.register('./combined-sw.js', {
			enabled: environment.production,
		}),
		AngularFireModule.initializeApp(environment.firebase),
		AngularFireMessagingModule,
	],
	providers: [
		{
			provide: RouteReuseStrategy,
			useClass: IonicRouteStrategy,
		},
		Storage
	],

	bootstrap: [AppComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
