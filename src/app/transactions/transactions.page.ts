import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ApiService } from '../services/api.service';
import { LoadingService } from '../services/loading.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { IonModal } from '@ionic/angular';
@Component({
	selector: 'app-transactions',
	templateUrl: './transactions.page.html',
	styleUrls: ['./transactions.page.scss'],
})
export class TransactionsPage implements OnInit {
	meta = {
		keyword: "transaction_id",
		search: "",
		order_by: "desc",
		per_page: 15,
		type: "new",
		page: 1,
		last_page: 0,
		customer_id: '',
		from_date: null,
		to_date: null,
		transaction_type: null,
		interval: null
	}
	filter_props = {
		from_date: '',
		to_date: '',
		transaction_type: "",
		interval: ''
	}
	customer: any
	transactions: any;
	errors: any;
	@ViewChild('filter') filter: IonModal;
	constructor(
		public loading: LoadingService,
		public api: ApiService,
		public router: Router,
		private storage: Storage,
	) { }

	ngOnInit() {
	}

	ionViewWillEnter() {
		this.storage.get('customer').then((data) => {
			if (data != null) {
				this.customer = data;
				this.meta.customer_id = this.customer.customer_id
				this.meta.page = 1;
				this.meta.last_page = 0;
				this.meta.type = 'new';
				this.getTransactions();
			}
		});
	}

	async getTransactions(event?) {
		if (this.filter) this.filter.dismiss();
		await this.api.httpCall('transactions/customerTransactions', this.meta).then((response) => {
			this.meta.last_page = response['meta'].last_page;
			if (this.meta.type == 'new')
				this.transactions = response['data'];
			else
				this.transactions = this.transactions.concat(response['data']);
			if (event)
				event.target.complete();
		}, (error) => {
			this.errors = error.error.errors;
		});
	}

	async loadData(event) {
		this.meta.type = 'append';
		this.meta.page++;
		await this.getTransactions(event);
	}

	async doRefresh(event) {
		this.meta.type = 'new';
		this.meta.page = 1;
		this.meta.last_page = 0
		this.transactions = null;
		this.meta.from_date = null;
		this.meta.to_date = null;
		this.meta.transaction_type = null
		await this.getTransactions(event);
	}

	async searchData(event) {
		this.meta.type = 'new';
		this.meta.page = 1;
		this.meta.last_page = 0;
		this.meta.search = event.target.value;
		await this.getTransactions();
	}

	async viewTransaction(transaction) {
		let navigationExtras: NavigationExtras = {
			state: {
				transaction: transaction,
				page:'transaction'
			},
		};
		this.router.navigate(["/transactions/view-transactions"], navigationExtras);
	}

	async filterTransactions() {
		// let filter = {
		// 	from_date: '',
		// 	to_date: '',
		// 	interval: '',
		// 	transaction_type: ''
		// }
		// const modal = await this.modalCtrl.create({
		// 	component: FilterPage,
		// 	componentProps: this.filter_props
		// });
		// modal.onDidDismiss().then((data) => {
		// 	filter = data.data
		// 	if (filter != null) {
		// 		this.meta.from_date = filter['from_date']
		// 		this.meta.to_date = filter['to_date']
		// 		this.meta.interval = filter['time_interval']
		// 		this.meta.transaction_type = filter['transaction_type']
		// 		this.filter_props.from_date = filter['from_date']
		// 		this.filter_props.to_date = filter['to_date']
		// 		this.filter_props.transaction_type = filter['transaction_type']
		// 		this.filter_props.interval = filter['time_interval']
		// 		this.getTransactions()
		// 	}
		// })
		// return await modal.present();
	}

	changeFormat(date_time) {
		return moment(date_time).format('DD-MM-YYYY : hh:mm a')
	}

	handleFromDate(e) {
		this.meta.from_date = moment(e.detail.value).format('DD-MM-YYYY');
		document.getElementById('Last 15 days').style.backgroundColor = '#FFFF'
		document.getElementById('Last 1 Month').style.backgroundColor = '#FFFF'
		document.getElementById('7').style.backgroundColor = '#FFFF'
	}

	handleToDate(e) {
		this.meta.to_date = moment(e.detail.value).format('DD-MM-YYYY');
		document.getElementById('Last 15 days').style.backgroundColor = '#FFFF'
		document.getElementById('Last 1 Month').style.backgroundColor = '#FFFF'
		document.getElementById('7').style.backgroundColor = '#FFFF'
	}

	handleInterval(interval) {
		document.getElementById('7').style.backgroundColor = '#FFFF'
		if (interval == 7) {
			var d = new Date();
			d.toLocaleString()
			d.setDate(d.getDate() - 7)
			d.toLocaleString()
			this.meta.from_date = moment(d.toLocaleString()).format('DD-MM-YYYY');
			this.meta.to_date = moment(new Date()).format('DD-MM-YYYY');
			this.meta.interval = 'last 7 days'
			document.getElementById('Last 15 days').style.backgroundColor = '#FFFF'
			document.getElementById('Last 1 Month').style.backgroundColor = '#FFFF'
			document.getElementById('7').style.backgroundColor = '#D3EAEB'
		}
		if (interval == 'Last 15 days') {
			var d = new Date();
			d.toLocaleString()
			d.setDate(d.getDate() - 15)
			d.toLocaleString()
			this.meta.from_date = moment(d.toLocaleString()).format('DD-MM-YYYY');
			this.meta.to_date = moment(new Date()).format('DD-MM-YYYY');
			this.meta.interval = interval
			document.getElementById('7').style.backgroundColor = '#FFFF'
			document.getElementById('Last 1 Month').style.backgroundColor = '#FFFF'
			document.getElementById('Last 15 days').style.backgroundColor = '#D3EAEB'
		}
		if (interval == 'Last 1 Month') {
			var d = new Date();
			d.toLocaleString()
			d.setMonth(d.getMonth() - 1)
			d.toLocaleString()
			this.meta.from_date = moment(d.toLocaleString()).format('DD-MM-YYYY');
			this.meta.to_date = moment(new Date()).format('DD-MM-YYYY');
			this.meta.interval = interval
			document.getElementById('Last 15 days').style.backgroundColor = '#FFFF'
			document.getElementById('7').style.backgroundColor = '#FFFF'
			document.getElementById('Last 1 Month').style.backgroundColor = '#D3EAEB'
		}
	}

	handleTransactionTypes(transaction_type) {
		this.meta.transaction_type = transaction_type
		if (transaction_type == 'Deposit') {
			document.getElementById('Deposit').style.backgroundColor = '#D3EAEB'
			document.getElementById('BuyIn').style.backgroundColor = '#FFFF'
			document.getElementById('CashOut').style.backgroundColor = '#FFFF'
			document.getElementById('Withdraw').style.backgroundColor = '#FFFF'
		}
		if (transaction_type == 'BuyIn') {
			document.getElementById('BuyIn').style.backgroundColor = '#D3EAEB'
			document.getElementById('CashOut').style.backgroundColor = '#FFFF'
			document.getElementById('Withdraw').style.backgroundColor = '#FFFF'
			document.getElementById('Deposit').style.backgroundColor = '#FFFF'
		}
		if (transaction_type == 'CashOut') {
			document.getElementById('CashOut').style.backgroundColor = '#D3EAEB'
			document.getElementById('Withdraw').style.backgroundColor = '#FFFF'
			document.getElementById('Deposit').style.backgroundColor = '#FFFF'
			document.getElementById('BuyIn').style.backgroundColor = '#FFFF'
		}
		if (transaction_type == 'Withdraw') {
			document.getElementById('Withdraw').style.backgroundColor = '#D3EAEB'
			document.getElementById('Deposit').style.backgroundColor = '#FFFF'
			document.getElementById('CashOut').style.backgroundColor = '#FFFF'
			document.getElementById('BuyIn').style.backgroundColor = '#FFFF'
		}
	}

	fromDatedidPresent(e) {
		if (e) {
			document.getElementById('from-date-img').style.transform = 'rotate(180deg)'
			document.getElementById('to-date').className = 'btn-deselected md button button-solid ion-activatable ion-focusable hydrated'
			console.log(document.getElementById('from-date').className = 'btn-selected md button button-solid ion-activatable ion-focusable hydrated')
		}
	}

	toDatedidPresent(e) {
		if (e) {
			document.getElementById('to-date-img').style.marginLeft = '10px'
			document.getElementById('to-date-img').style.transform = 'rotate(180deg)'
			document.getElementById('from-date').className = 'btn-deselected md button button-solid ion-activatable ion-focusable hydrated'
			document.getElementById('to-date').className = 'btn-selected md button button-solid ion-activatable ion-focusable hydrated'
		}
	}

	fromDateionPopoverDidDismiss(e) {
		document.getElementById('from-date-img').style.transform = 'rotate(90deg)'
	}

	toDateionPopoverDidDismiss(e) {
		document.getElementById('to-date-img').style.transform = 'rotate(90deg)'
	}

	onWillDismiss(e) {
		if (e.detail.role == "backdrop") {
			this.meta.from_date = null;
			this.meta.to_date = null;
			this.meta.transaction_type = null;
			this.meta.interval = null;
		}
	}
}
