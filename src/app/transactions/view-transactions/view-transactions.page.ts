import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController, ModalController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';

@Component({
	selector: 'app-view-transactions',
	templateUrl: './view-transactions.page.html',
	styleUrls: ['./view-transactions.page.scss'],
})
export class ViewTransactionsPage implements OnInit {
	transaction: any;
	customer: any;
	errors: any
	page: any
	meta = {
		customer_id: null,
		branch_id: null,
		transaction_id: null
	}
	constructor(
		public toastController: ToastController,
		public loading: LoadingService,
		public api: ApiService,
		private storage: Storage,
		public modalCtrl: ModalController,
		private route: ActivatedRoute,
		public router: Router,
	) {
		this.route.queryParams.subscribe(params => {
			if (this.router.getCurrentNavigation().extras.state) {
				this.transaction = this.router.getCurrentNavigation().extras.state['transaction'];
				this.page = this.router.getCurrentNavigation().extras.state['page'];
				this.meta.customer_id = this.transaction?.customer_id
			}
		});
	}

	ngOnInit() {
	}
	async ionViewWillEnter() {
		await this.storage.get("customer").then((data) => {
			if (data !== null) {
				this.customer = data;
				this.meta.customer_id = data.customer_id;
				// this.getTransaction();
			}
		});
	}

	async getTransaction() {
		await this.loading.open();
		await this.api.httpCall('transactions/getCustomerWallet', this.meta).then((response) => {
			this.loading.close();
		}, (error) => {
			this.loading.close()
			this.errors = error.error.errors;
		});
	}

	changeFormat(date_time) {
		return moment(date_time).format('DD-MM-YYYY : hh:mm a')
	}

	close() {
		this.router.navigateByUrl('/transactions')
	}
	download(attachment) {
		if (attachment != null) {
			window.open(attachment)
		} else {
			alert('No file found.')
		}
	}

	viewReceipt() {
		window.open('https://poker.hexaqore.com/api/downloadTransaction?' + 'transaction_id=' + this.transaction.transaction_id, '_blank')
	}
}
