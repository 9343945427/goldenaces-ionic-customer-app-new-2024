import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { ApiService } from '../services/api.service';
import { Storage } from '@ionic/storage';
import { AlertController, NavController } from '@ionic/angular';

@Component({
	selector: 'app-email',
	templateUrl: './email.page.html',
	styleUrls: ['./email.page.scss'],
})
export class EmailPage implements OnInit {
	mobile_no: any;
	emailForm: FormGroup;
	validations = {
		'email': [
			{ type: 'required', message: 'Email address field is required.' },
			{ type: 'pattern', message: 'Enter a valid email address.' },
		],
	};
	constructor(
		public loading: LoadingService,
		public api: ApiService,
		private route: ActivatedRoute,
		public router: Router,
		private storage: Storage,
		public alertController: AlertController,
	) {
		this.emailForm = new FormGroup({
			'mobile_no': new FormControl('', Validators.compose([
				Validators.required,
				Validators.pattern('^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$'),
			])),
			'email': new FormControl('', Validators.compose([
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			])),
		});
		this.route.queryParams.subscribe(params => {
			if (this.router.getCurrentNavigation().extras.state) {
				this.mobile_no = this.router.getCurrentNavigation().extras.state['mobile_no'];
				this.emailForm.controls["mobile_no"].setValue(this.mobile_no);
			}
		});
	}

	ngOnInit() {
	}

	verifyEmail() {
		let navigationExtras: NavigationExtras = {
			state: {
				mobile_no: this.mobile_no,
				email: this.emailForm.value.email
			}
		};
		this.router.navigate(['/referral-code'], navigationExtras);
	}
}
