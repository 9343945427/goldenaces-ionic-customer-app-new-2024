import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { ApiService } from '../services/api.service';
import { Storage } from '@ionic/storage';
import { AlertController, NavController } from '@ionic/angular';

@Component({
	selector: 'app-referral-code',
	templateUrl: './referral-code.page.html',
	styleUrls: ['./referral-code.page.scss'],
})
export class ReferralCodePage implements OnInit {
	errors: any;
	referralForm: FormGroup;
	isModalOpen = false;
	canDismiss = false;
	terms: any;
	click_status = 'true'
	validations = {
		'email': [
			{ type: 'required', message: 'Email address field is required.' },
			{ type: 'pattern', message: 'Enter a valid email address.' },
		],
	};
	constructor(
		public loading: LoadingService,
		public api: ApiService,
		private route: ActivatedRoute,
		public router: Router,
		private storage: Storage,
		public alertController: AlertController,
		private navCtrl: NavController
	) {
		this.referralForm = new FormGroup({
			'mobile_no': new FormControl('', Validators.compose([
				Validators.required,
				Validators.pattern('^([0|\+[0-9]{1,5})?([6-9][0-9]{9})$'),
			])),
			'email': new FormControl('', Validators.compose([
				Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
			])),
			'referral_code': new FormControl('', Validators.compose([

			])),
		});
		this.route.queryParams.subscribe(params => {
			if (this.router.getCurrentNavigation().extras.state) {
				this.referralForm.controls["mobile_no"].setValue(this.router.getCurrentNavigation().extras.state['mobile_no']);
				this.referralForm.controls["email"].setValue(this.router.getCurrentNavigation().extras.state['email']);
			}
		});
	}

	ngOnInit() {
		this.getLoginTerms();
		this.click_status = 'true'
	}

	async getLoginTerms() {
		await this.api.open('getLoginTerm', '').then((response) => {
			this.terms = response['data']
			this.isModalOpen = true
		}, (error) => {
			this.errors = error.error.errors;
		});
	}

	async getLoginTerm() {
		await this.api.open('getLoginTerm', '').then((response) => {
			this.terms = response['data']
			this.isModalOpen = true
			console.log(this.isModalOpen)
		}, (error) => {
			this.errors = error.error.errors;
		});
	}

	onChanged(event?) {
		event.target.checked == true ? this.click_status = 'true' : this.click_status = 'false'
		event.target.checked == false ? this.isModalOpen = false : this.isModalOpen = true
		console.log(event.target.checked, this.click_status)
	}

	onWillDismiss(e) {
		if (this.click_status == 'true') {
			this.isModalOpen = true
		}
	}

	async register() {
		await this.loading.open();
		await this.api.open('registerCustomer', this.referralForm.value).then((response) => {
			this.loading.close()
			this.storage.set('customer', response['customer']);
			this.storage.set('token', response['token']);
			this.navCtrl.navigateRoot('/home');
		}, (error) => {
			this.loading.presentAlert('Message', error.error.message)
			this.errors = error.error.errors;
			this.loading.close();
		});
	}
}