import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReferralCodePage } from './referral-code.page';

describe('ReferralCodePage', () => {
  let component: ReferralCodePage;
  let fixture: ComponentFixture<ReferralCodePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ReferralCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
