import { Injectable } from '@angular/core';
import { CanLoad } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Injectable({
	providedIn: 'root'
})
export class LoginGuard implements CanLoad {
	constructor(
		public navCtrl: NavController,
		private storage: Storage,
	) {
	}

	async canLoad() {
		let state = 0;
		await this.storage.get('customer').then((data) => {
			if (data != null) {
				state = 1;
			}
		});
		if (state == 1) {
			this.navCtrl.navigateRoot('/home');
			return false;
		}
		else {
			return true;
		}
	}
}