import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RefundReturnPageRoutingModule } from './refund-return-routing.module';

import { RefundReturnPage } from './refund-return.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RefundReturnPageRoutingModule
  ],
  declarations: [RefundReturnPage]
})
export class RefundReturnPageModule {}
