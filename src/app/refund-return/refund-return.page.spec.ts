import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RefundReturnPage } from './refund-return.page';

describe('RefundReturnPage', () => {
  let component: RefundReturnPage;
  let fixture: ComponentFixture<RefundReturnPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(RefundReturnPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
