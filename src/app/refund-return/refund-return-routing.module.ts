import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RefundReturnPage } from './refund-return.page';

const routes: Routes = [
  {
    path: '',
    component: RefundReturnPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RefundReturnPageRoutingModule {}
