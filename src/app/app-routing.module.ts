import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginGuard } from './guards/login.guard';
const routes: Routes = [
	{
		path: 'signup',
		loadChildren: () => import('./signup/signup.module').then(m => m.SignupPageModule),
		canLoad: [LoginGuard]
	},
	{
		path: '',
		redirectTo: '/signup',
		pathMatch: 'full'
	},
	{
		path: 'otp',
		loadChildren: () => import('./otp/otp.module').then(m => m.OtpPageModule),
		canLoad: [LoginGuard]
	},
	{
		path: 'email',
		loadChildren: () => import('./email/email.module').then(m => m.EmailPageModule),
		
	},
	{
		path: 'home',
		loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
		canLoad: [AuthGuard]
	},
	{
		path: 'wallet',
		loadChildren: () => import('./wallet/wallet.module').then(m => m.WalletPageModule),
		canLoad: [AuthGuard]
	},
	{
		path: 'transactions',
		loadChildren: () => import('./transactions/transactions.module').then(m => m.TransactionsPageModule),
		canLoad: [AuthGuard]
	},
	{
		path: 'profile',
		loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule),
		canLoad: [AuthGuard]
	},
	{
		path: 'referral-code',
		loadChildren: () => import('./referral-code/referral-code.module').then(m => m.ReferralCodePageModule),
	},
	{
		path: 'terms-conditions',
		loadChildren: () => import('./terms-conditions/terms-conditions.module').then(m => m.TermsConditionsPageModule)
	},
  {
    path: 'privacy',
    loadChildren: () => import('./privacy/privacy.module').then( m => m.PrivacyPageModule)
  },  {
    path: 'policies',
    loadChildren: () => import('./policies/policies.module').then( m => m.PoliciesPageModule)
  },
  {
    path: 'refund-return',
    loadChildren: () => import('./refund-return/refund-return.module').then( m => m.RefundReturnPageModule)
  },



];
@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
	],
	exports: [RouterModule]
})
export class AppRoutingModule { }
