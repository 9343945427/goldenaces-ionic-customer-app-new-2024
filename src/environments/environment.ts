// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	// apiUrl:'https://admin.goldenaces.club/api/customer/',
	// assetUrl: 'https://admin.goldenaces.club/storage/',

	apiUrl: 'https://poker.hexaqore.com/api/customer/',
	assetUrl: 'https://poker.hexaqore.com/storage/',

	// apiUrl: "http://192.168.0.213/golden-aces-laravel-app/public/api/customer/",
	// assetUrl: 'https://admin.goldenaces.club/storage/',

	// apiUrl: "http://localhost/goldenaces.club/laravel/public/api/customer/",
	// assetUrl: 'https://admin.goldenaces.club/storage/',

	firebase: {
		apiKey: "AIzaSyDEyD24BnJpI9v2udpgoZN7GR5lrgki580",
		authDomain: "golden-aces-10ce1.firebaseapp.com",
		projectId: "golden-aces-10ce1",
		storageBucket: "golden-aces-10ce1.appspot.com",
		messagingSenderId: "58116634487",
		appId: "1:58116634487:web:b6e574bdedb20880e99372"
	}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
