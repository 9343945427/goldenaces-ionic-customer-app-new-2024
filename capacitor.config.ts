import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
	appId: 'club.goldenaces.app',
	appName: 'Golden Aces',
	webDir: 'www',
	cordova: {},
	plugins: {
		SplashScreen: {
			"launchAutoHide": true,
			"backgroundColor": "#000000",
			"splashFullScreen": true,
			"splashImmersive": true,
			"useDialog" : false
		},
		PushNotifications :{
			presentationOptions:['alert', 'badge', 'sound'],
		}
	},
};

export default config;
